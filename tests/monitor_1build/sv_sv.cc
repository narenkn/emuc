#include <queue>
#include <iostream>
#include <unordered_map>
#include <boost/asio.hpp>
#include "scCommon.hh"
#if EMUC_SERVER
  #include "server.hh"
#endif
#if EMUC_CLIENT
  #include "client.hh"
#endif

#include <cstring>

#include "svbit.hh"
#include "shash.hh"

class SdpReqBus : public TransBase {
public:
  std::uint32_t* const _dptr;
  svBitTp<8> counter1{_dptr};
  svBitTp<8> counter2{_dptr+SV_SIZE(8)};
  svBitTp<8> counter3{_dptr+SV_SIZE(8)+SV_SIZE(8)};
  static constexpr std::size_t
  DATA_U32_SZ() { return SV_SIZE(8)+SV_SIZE(8)+SV_SIZE(8); }
  static constexpr std::size_t
  DATA_U8_SZ() { return DATA_U32_SZ()*sizeof(std::uint32_t); }
  SdpReqBus():
    TransBase{DATA_U8_SZ()},
    _dptr{(std::uint32_t*)(_data+sizeof(TransHeader))} {
  }
  SdpReqBus(char *d): SdpReqBus() {
    std::memcpy(_dptr, d, DATA_U8_SZ());
  }
  SdpReqBus& operator=(const SdpReqBus& o) { /* copy assignment */
    std::memcpy(_dptr, o._dptr, DATA_U8_SZ());
  }
  SdpReqBus(const SdpReqBus& o):
    SdpReqBus() { /* copy constructor */
    std::memcpy(_data, o._data, getWrPtrSz());
  }
};

static svScope emucScope;
extern "C" void emuc_req_get_trigger();

template<class Bus>
class EmuTransactor : public Pipe
{
public:
  std::queue<Bus> received;
  EmuTransactor(const std::string& PipeIdStr) :
    Pipe{CRC32_STRING(PipeIdStr), Bus::DATA_U8_SZ()}
  {
    //std::cout << "EmuTransactor() called\n";
    connect();
  }
  void receive(char *d)
  {
    received.emplace(d);
    if (not emucScope) {
      emucScope = svGetScopeFromName("emuc");
    }
    svSetScope(emucScope);
    emuc_req_get_trigger();
    //std::cout << "EmuTransactor::receive called\n";
  }
};
std::unordered_map<std::uint32_t, EmuTransactor<SdpReqBus>> ReqTransactors;

extern "C"
void emuc_req_put(std::uint32_t pipeId, char *p)
{
  auto it = ReqTransactors.find(pipeId);
  if (it == ReqTransactors.end()) {
    std::cerr << "Error: Attempt to send through pipeId:" << pipeId << "\n";
    return;
  }

  std::shared_ptr<SdpReqBus> req_trans{std::make_shared<SdpReqBus>(p)};
  it->second.send(req_trans);
}

extern "C"
void emuc_req_get(std::uint32_t pipeId, char *p)
{
  auto it = ReqTransactors.find(pipeId);
  if (it == ReqTransactors.end()) return;

  if (0 == it->second.received.size()) return;

  auto pack = it->second.received.front();
  std::memcpy(p, pack._data, pack.DATA_U8_SZ()+sizeof(TransHeader));
  it->second.received.pop();
}

extern "C"
std::uint32_t emuc_req_num(std::uint32_t pipeId)
{
  //std::cout << "emuc_req_num pipeId:" << std::hex << pipeId << "\n";
  auto it = ReqTransactors.find(pipeId);
  return (it == ReqTransactors.end()) ? 0 : it->second.received.size();
}

extern "C"
std::uint32_t emuc_req_register(const char* path)
{
  std::uint32_t pipeId = CRC32_STRING(path);

  //std::cout << "emuc_req_num pipeId:" << std::hex << pipeId << "\n";
  auto it = ReqTransactors.find(pipeId);
  if (it == ReqTransactors.end()) {
    ReqTransactors.emplace(pipeId, path);
    //std::cout << std::hex << " CRC:" << CRC32_STR("abcd")<< " pipeId:" << pipeId << "\n";
    it = ReqTransactors.find(pipeId);
  } else {
    std::cout << "Warning: pipe(" << path << ") : Already exists!!\n";
  }

  return pipeId;
}
