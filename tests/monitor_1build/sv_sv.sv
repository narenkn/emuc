
package emuc;
   typedef struct {
      bit [31:0]  pipeId;
      bit [31:0]  byteSize;
   } Header;
   typedef struct {
      emuc::Header h;
      bit [7:0]  counter1;
      bit [7:0]  counter2;
      bit [7:0]  counter3;
   } REQ;
   parameter REQ_size = 12;
   import "DPI-C" function int unsigned emuc_hash( string path );
   export "DPI-C" function emuc_req_get_trigger;
   event          emuc_req_received;
   function void emuc_req_get_trigger();
      -> emuc_req_received;
   endfunction // trigger
   import "DPI-C" function void emuc_req_put( int unsigned id, inout REQ dat );
   import "DPI-C" function void emuc_req_get( int unsigned id, inout REQ dat );
   import "DPI-C" function int unsigned emuc_req_num(int unsigned id);
   import "DPI-C" function int unsigned emuc_req_register(string path);
endpackage

// Fixed delay fifo
module EmuFifoOut
#( 
    parameter FIFO_W = 8, 
    parameter FIFO_S = 16
)
(
    input               Clk,
    input               Reset,
    input [FIFO_W-1:0]  FifoIn,
    input               PushIn,
    output              Error
);

   parameter FPTR_W = $clog2(FIFO_S);

   reg [FIFO_W-1:0]        Fifo[FIFO_S-1:0];
   reg [FPTR_W:0]          wptr, wptr_l;

   integer                 i;
   initial
     for (i=0; i < FIFO_S; i=i+1)
       Fifo[i] <= {FIFO_W{1'b0}};

   always @(posedge Clk)
     if (Reset) begin
        wptr <= {(FPTR_W+1){1'b0}};
        wptr_l <= wptr;
        Error <= 1'b0;
     end else begin
        if (PushIn)
          wptr <= wptr + {{FPTR_W{1'b0}}, {1'b1}};
        Error <= Error | (PushIn & Full);
     end

   always @(posedge Clk)
     if (PushIn)
       Fifo[wptr[FPTR_W-1:0]] <= FifoIn;

   wire Full = (wptr[FPTR_W-1:0] == (wptr_l[FPTR_W-1:0]+1)) &
        (wptr[FPTR_W] ^ wptr_l[FPTR_W]);

`ifdef ABV
ovl_never #(.msg("Attempting to push while fifo is full")) FifoOverflow(.clock(Clk), .reset(~Reset), .enable(`ABV_HWASSERT_DIS), .test_expr(Error),. fire());
`endif

endmodule
