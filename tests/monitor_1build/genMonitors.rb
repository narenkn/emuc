require 'erubis'
require_relative "../../gen_files/badami/local_features"

sdpSigTemplate = %{

{
  Req: {
    _feature: true,
    ReqTag: [true, local_features(:<%=PORT%>_SDP_TAG__SIZE)],
    ReqAddr: [true, local_features(:<%=PORT%>_SDP_ADDR__MSB)+1],
    ReqIO: [local_features(:<%=PORT%>_SDP_REQIO_PRESENT), 1],
    ReqLen: [true, local_features(:<%=PORT%>_SDP_LEN__SIZE)],
    ReqLock: [local_features(:<%=PORT%>_SDP_REQLOCK_PRESENT), 1],
    ReqAck: [local_features(:<%=PORT%>_SDP_REQACK_PRESENT), 1],
    ReqAttr: [local_features(:<%=PORT%>_SDP_REQATTR_PRESENT), local_features(:<%=PORT%>_SDP_ATTR__SIZE)],
    ReqQosPriority: [local_features(:<%=PORT%>_SDP_REQQOSPRIORITY_PRESENT), local_features(:<%=PORT%>_SDP_QOS_PRI__SIZE)],
    ReqQosForward: [local_features(:<%=PORT%>_SDP_REQQOSFORWARD_PRESENT), local_features(:<%=PORT%>_SDP_QOS_FWD__SIZE)],
    ReqCmd: [true, local_features(:<%=PORT%>_SDP_CMD__SIZE)],
    ReqDomain: [local_features(:<%=PORT%>_SDP_REQDOMAIN_PRESENT), 1],
    ReqVfidValid: [local_features(:<%=PORT%>_SDP_REQVFIDVALID_PRESENT), 1],
    ReqIbs: [true, 1],
    ReqTMZ: [local_features(:<%=PORT%>_SDP_REQTMZ_PRESENT), 1],
    ReqVirtAddr: [local_features(:<%=PORT%>_SDP_REQVIRTADDR_PRESENT), 1],
    ReqUnitID: [local_features(:<%=PORT%>_SDP_REQUNITID_PRESENT), local_features(:<%=PORT%>_SDP_UNITID__SIZE)],
    ReqVfid: [local_features(:<%=PORT%>_SDP_REQVFID_PRESENT), local_features(:<%=PORT%>_SDP_REQ_VFID__SIZE)],
    ReqStreamID: [local_features(:<%=PORT%>_SDP_REQSTREAMID_PRESENT), local_features(:<%=PORT%>_SDP_STREAMID__SIZE)],
    ReqSecLevel: [local_features(:<%=PORT%>_SDP_REQSECLEVEL_PRESENT), local_features(:<%=PORT%>_SDP_SECURITY_LEVEL__SIZE)],
    ReqPassPW: [local_features(:<%=PORT%>_SDP_REQPASSPW_PRESENT), 1],
    ReqBlockLevel: [local_features(:<%=PORT%>_SDP_REQBLOCKLEVEL_PRESENT), local_features(:<%=PORT%>_SDP_REQ_BLOCK_LEVEL__SIZE)],
    ReqRspPassPW: [local_features(:<%=PORT%>_SDP_REQRSPPASSPW_PRESENT), 1],
    ReqVC: [local_features(:<%=PORT%>_SDP_REQVC_PRESENT), local_features(:<%=PORT%>_SDP_VC__SIZE)],
    ReqChain: [local_features(:<%=PORT%>_SDP_REQCHAIN_PRESENT), 1],
    ReqParity: [local_features(:<%=PORT%>_SDP_REQPARITY_PRESENT), 1],
    ReqChanAB: [local_features(:<%=PORT%>_SDP_REQCHANAB_PRESENT), 1],
  },
  OrigData: {
    _feature: local_features(:<%=PORT%>_SDP_ORIGDATAVLD_PRESENT),
    OrigDataChan: [local_features(:<%=PORT%>_SDP_ORIGDATACHAN_PRESENT), 1],
    OrigData: [local_features(:<%=PORT%>_SDP_ORIGDATA_PRESENT), local_features(:<%=PORT%>_SDP_DATA__SIZE)],
    OrigDataBytEn: [local_features(:<%=PORT%>_SDP_ORIGDATABYTEN_PRESENT), local_features(:<%=PORT%>_SDP_BYTEN__SIZE)],
    OrigDataLast: [local_features(:<%=PORT%>_SDP_ORIGDATALAST_PRESENT), 1],
    OrigDataError: [local_features(:<%=PORT%>_SDP_ORIGDATAERROR_PRESENT), 1],
    OrigDataDbgMsk: [local_features(:<%=PORT%>_SDP_ORIGDATADBGMSK_PRESENT), 1],
    OrigDataOffset: [local_features(:<%=PORT%>_SDP_ORIGDATAOFFSET_PRESENT), local_features(:<%=PORT%>_SDP_ORIG_DAT_OFFSET__SIZE)],
    OrigDataVC: [local_features(:<%=PORT%>_SDP_ORIGDATAVC_PRESENT), local_features(:<%=PORT%>_SDP_VC__SIZE)],
    OrigDataParity: [local_features(:<%=PORT%>_SDP_ORIGDATAPARITY_PRESENT), local_features(:<%=PORT%>_SDP_DATA_PARITY__SIZE)],
    OrigDataMetaParity: [local_features(:<%=PORT%>_SDP_ORIGDATAMETAPARITY_PRESENT), 1],
    OrigDataChanAB: [local_features(:<%=PORT%>_SDP_ORIGDATACHANAB_PRESENT), 1],
    OrigDataUser: [local_features(:<%=PORT%>_SDP_ORIGDATAUSER_PRESENT), local_features(:<%=PORT%>_SDP_ORIG_DATA_USER__SIZE)],
  },
  RdRsp: {
    _feature: local_features(:<%=PORT%>_SDP_RDRSPVLD_PRESENT),
    RdRspTag: [local_features(:<%=PORT%>_SDP_RDRSPTAG_PRESENT), local_features(:<%=PORT%>_SDP_TAG__SIZE)],
    RdRspUnitID: [local_features(:<%=PORT%>_SDP_RDRSPUNITID_PRESENT), local_features(:<%=PORT%>_SDP_UNITID__SIZE)],
    RdRspVC: [local_features(:<%=PORT%>_SDP_RDRSPVC_PRESENT), local_features(:<%=PORT%>_SDP_VC__SIZE)],
    RdRspPassPW: [local_features(:<%=PORT%>_SDP_RDRSPPASSPW_PRESENT), 1],
    RdRspDbgMsk: [local_features(:<%=PORT%>_SDP_RDRSPDBGMSK_PRESENT), 1],
    RdRspDataVld: [local_features(:<%=PORT%>_SDP_RDRSPDATAVLD_PRESENT), 1],
    RdRspData: [local_features(:<%=PORT%>_SDP_RDRSPDATA_PRESENT), local_features(:<%=PORT%>_SDP_RD_RSP_DATA__SIZE)],
    RdRspDataParity: [local_features(:<%=PORT%>_SDP_RDRSPDATAPARITY_PRESENT), local_features(:<%=PORT%>_SDP_RD_RSP_DATA_PARITY__SIZE)],
    RdRspDataStatus: [local_features(:<%=PORT%>_SDP_RDRSPDATASTATUS_PRESENT), local_features(:<%=PORT%>_SDP_RD_RSP_DAT_STATUS__SIZE)],
    RdRspDataStatusParity: [local_features(:<%=PORT%>_SDP_RDRSPDATASTATUSPARITY_PRESENT), 1],
    RdRspParity: [local_features(:<%=PORT%>_SDP_RDRSPPARITY_PRESENT), 1],
    RdRspDelay: [local_features(:<%=PORT%>_SDP_RDRSPDELAY_PRESENT), 1],
    RdRspStatus: [local_features(:<%=PORT%>_SDP_RDRSPSTATUS_PRESENT), local_features(:<%=PORT%>_SDP_RD_RSP_STATUS__SIZE)],
    RdRspOffset: [local_features(:<%=PORT%>_SDP_RDRSPOFFSET_PRESENT), local_features(:<%=PORT%>_SDP_RD_RSP_OFFSET__SIZE)],
    RdRspLast: [local_features(:<%=PORT%>_SDP_RDRSPLAST_PRESENT), 1],
    RdRspChanAB: [local_features(:<%=PORT%>_SDP_RDRSPCHANAB_PRESENT), 1],
    RdRspVld: [local_features(:<%=PORT%>_SDP_RDRSPVLD_PRESENT), 1],
    RdRspRdy: [local_features(:<%=PORT%>_SDP_RDRSPRDY_PRESENT), 1],
    RdRspCreditChanAB: [local_features(:<%=PORT%>_SDP_RDRSPCREDITCHANAB_PRESENT), 1],
    RdRspCreditType: [local_features(:<%=PORT%>_SDP_RDRSPCREDITTYPE_PRESENT), 1],
    RdRspCreditVC: [local_features(:<%=PORT%>_SDP_RDRSPCREDITVC_PRESENT), local_features(:<%=PORT%>_SDP_VC__SIZE)],
    RdRspCreditParity: [local_features(:<%=PORT%>_SDP_RDRSPCREDITPARITY_PRESENT), 1],
    RdRspCreditVld: [local_features(:<%=PORT%>_SDP_RDRSPCREDITVLD_PRESENT), 1],
    RdRspCreditRdy: [local_features(:<%=PORT%>_SDP_RDRSPCREDITRDY_PRESENT), 1],
    RdRspUser: [local_features(:<%=PORT%>_SDP_RDRSPUSER_PRESENT), local_features(:<%=PORT%>_SDP_RD_RSP_USER__SIZE)],
    RdRspDataUser: [local_features(:<%=PORT%>_SDP_RDRSPDATAUSER_PRESENT), local_features(:<%=PORT%>_SDP_RD_RSP_DAT_USER__SIZE)],
  },
  WrRsp: {
    _feature: local_features(:<%=PORT%>_SDP_WRRSPVLD_PRESENT),
    WrRspTag: [local_features(:<%=PORT%>_SDP_WRRSPTAG_PRESENT), local_features(:<%=PORT%>_SDP_TAG__SIZE)],
    WrRspUnitID: [local_features(:<%=PORT%>_SDP_WRRSPUNITID_PRESENT), local_features(:<%=PORT%>_SDP_UNITID__SIZE)],
    WrRspStatus: [local_features(:<%=PORT%>_SDP_WRRSPSTATUS_PRESENT), local_features(:<%=PORT%>_SDP_WR_RSP_STATUS__SIZE)],
    WrRspVC: [local_features(:<%=PORT%>_SDP_WRRSPVC_PRESENT), local_features(:<%=PORT%>_SDP_VC__SIZE)],
    WrRspPassPW: [local_features(:<%=PORT%>_SDP_WRRSPPASSPW_PRESENT), 1],
    WrRspParity: [local_features(:<%=PORT%>_SDP_WRRSPPARITY_PRESENT), 1],
    WrRspChanAB: [local_features(:<%=PORT%>_SDP_WRRSPCHANAB_PRESENT), 1],
    WrRspVld: [local_features(:<%=PORT%>_SDP_WRRSPVLD_PRESENT), 1],
    WrRspRdy: [local_features(:<%=PORT%>_SDP_WRRSPRDY_PRESENT), 1],
    WrRspCreditChanAB: [local_features(:<%=PORT%>_SDP_WRRSPCREDITCHANAB_PRESENT), 1],
    WrRspCreditType: [local_features(:<%=PORT%>_SDP_WRRSPCREDITTYPE_PRESENT), 1],
    WrRspCreditVC: [local_features(:<%=PORT%>_SDP_WRRSPCREDITVC_PRESENT), local_features(:<%=PORT%>_SDP_VC__SIZE)],
    WrRspCreditParity: [local_features(:<%=PORT%>_SDP_WRRSPCREDITPARITY_PRESENT), 1],
    WrRspCreditVld: [local_features(:<%=PORT%>_SDP_WRRSPCREDITVLD_PRESENT), 1],
    WrRspCreditRdy: [local_features(:<%=PORT%>_SDP_WRRSPCREDITRDY_PRESENT), 1],
    WrRspUser: [local_features(:<%=PORT%>_SDP_WRRSPUSER_PRESENT), local_features(:<%=PORT%>_SDP_WR_RSP_USER__SIZE)],
  },
  Ack: {
    _feature: local_features(:<%=PORT%>_SDP_ACKVLD_PRESENT),
    AckVld: [local_features(:<%=PORT%>_SDP_ACKVLD_PRESENT), 1],
    AckRdy: [local_features(:<%=PORT%>_SDP_ACKRDY_PRESENT), 1],
    AckTag: [local_features(:<%=PORT%>_SDP_ACKTAG_PRESENT), local_features(:<%=PORT%>_SDP_TAG__SIZE)],
    AckUnitID: [local_features(:<%=PORT%>_SDP_ACKUNITID_PRESENT), local_features(:<%=PORT%>_SDP_UNITID__SIZE)],
    AckCancel: [local_features(:<%=PORT%>_SDP_ACKCANCEL_PRESENT), 1],
    AckParity: [local_features(:<%=PORT%>_SDP_ACKPARITY_PRESENT), 1],
    AckCreditVld: [local_features(:<%=PORT%>_SDP_ACKCREDITVLD_PRESENT), 1],
    AckCreditRdy: [local_features(:<%=PORT%>_SDP_ACKCREDITRDY_PRESENT), 1],
  },
  PrbReq: {
    _feature: local_features(:<%=PORT%>_SDP_PRBVLD_PRESENT),
    PrbTag: [local_features(:<%=PORT%>_SDP_PRBTAG_PRESENT), local_features(:<%=PORT%>_SDP_TAG__SIZE)],
    PrbAddr: [local_features(:<%=PORT%>_SDP_PRBADDR_PRESENT), local_features(:<%=PORT%>_SDP_ADDR__SIZE)],
    PrbAction: [local_features(:<%=PORT%>_SDP_PRBACTION_PRESENT), local_features(:<%=PORT%>_SDP_PROBE_ACTION__SIZE)],
    PrbRD: [local_features(:<%=PORT%>_SDP_PRBRD_PRESENT), local_features(:<%=PORT%>_SDP_PROBE_RD__SIZE)],
    PrbChain: [local_features(:<%=PORT%>_SDP_PRBCHAIN_PRESENT), 1],
    PrbVld: [local_features(:<%=PORT%>_SDP_PRBVLD_PRESENT), 1],
    PrbRdy: [local_features(:<%=PORT%>_SDP_PRBRDY_PRESENT), 1],
    PrbParity: [local_features(:<%=PORT%>_SDP_PRBPARITY_PRESENT), 1],
    PrbCreditVld: [local_features(:<%=PORT%>_SDP_PRBCREDITVLD_PRESENT), 1],
    PrbCreditRdy: [local_features(:<%=PORT%>_SDP_PRBCREDITRDY_PRESENT), 1],
  },
  PrbRsp: {
    _feature: local_features(:<%=PORT%>_SDP_PRBRSPVLD_PRESENT),
    PrbRspTag: [local_features(:<%=PORT%>_SDP_PRBRSPTAG_PRESENT), local_features(:<%=PORT%>_SDP_TAG__SIZE)],
    PrbRspStatus: [local_features(:<%=PORT%>_SDP_PRBRSPSTATUS_PRESENT), local_features(:<%=PORT%>_SDP_PROBE_RSP_STATUS__SIZE)],
    PrbRspParity: [local_features(:<%=PORT%>_SDP_PRBRSPPARITY_PRESENT), 1],
    PrbRspVld: [local_features(:<%=PORT%>_SDP_PRBRSPVLD_PRESENT), 1],
    PrbRspRdy: [local_features(:<%=PORT%>_SDP_PRBRSPRDY_PRESENT), 1],
    PrbRspCreditVld: [local_features(:<%=PORT%>_SDP_PRBRSPCREDITVLD_PRESENT), 1],
    PrbRspCreditRdy: [local_features(:<%=PORT%>_SDP_PRBRSPCREDITRDY_PRESENT), 1],
  },
}

}
erSigTemplate = Erubis::Eruby.new(sdpSigTemplate);

$port = :CPU;

File.open('abcd', 'w') do |f|
  eo = erSigTemplate.result(:PORT => $port);
  sdpChannelSignals = eval(eo);

  first_sig = true;
  f.write("module monSdp#{$port} (\n");
  sdpChannelSignals.each_pair do |grp, grps|
    next if not grps[:_feature];
    f.write(",\n") if not first_sig;
    f.write("\n  /* Group #{grp} */\n");
    first_sig = true;
    grps.each_pair do |sig, sigf|
      next if (:_feature == sig);
      next if not sigf[0];
      f.write(",\n") if not first_sig;
      (sigf[1]==1) ? f.write("  input #{sig}") :
        f.write("  input [#{sigf[1]-1}:0] #{sig}");
      first_sig = false;
    end
  end
  f.write(" );\n") if not first_sig;
  f.write("endmodule\n");
end
