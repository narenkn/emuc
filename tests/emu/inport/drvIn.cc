#include <chrono>
#include <thread>
#include <cstring>
#include <vector>
#include <queue>
#include <iostream>
#include <memory>
#include <unordered_map>
#include <boost/asio.hpp>
#include "shash.hh"

namespace std {
  template<typename T, typename... Args>
  std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
  }
}


#ifdef VCS
  void*
  scemi_mem_c_handle(const char *p) { return nullptr; }
  void
  scemi_mem_get_size(void* t_this, unsigned int *w, long long unsigned int *s) { *w = 64; *s = 100; }
  void scemi_mem_put_block(void* t_this, std::uint32_t start, std::uint32_t size, void *src) { }
  void scemi_mem_get_block(void* t_this, std::uint32_t start, std::uint32_t size, void *dest) { }
#else
  #include "tbxbindings.h"
  #include "svdpi.h"
  #include "stdio.h"
  #include "stdlib.h"
  #include "scemi_dmi.h"
#endif

#define STR_H(l) #l
#define STR(l) STR_H(l)

struct WriteBus {
  std::uint32_t inVal;
  WriteBus() { inVal = 0; }
  void toInt32(std::uint32_t *b) {
    if (b[1] & 0x80000000) {
      throw [&](){ std::stringstream ss; ss << __FILE__ "(" STR(__LINE__) "): b[1]:" << b[1] << "\n"; return ss.str(); };
    }
    b[0] = inVal++;
    b[1] |= 0x80000000;
  }
};

WriteBus wb;

#include "scCommon.hh"
#include "emuPorts.hh"

EmuInPort<WriteBus, 256, 64> inPort{"testbench.getIn", CRC32_STR("testbench.getIn")};

static bool first = true;
extern "C" void
sw_poll()
{
  try {
    if (first) inPort.connect(), first = false;
//    while (inPort.send(wb)) {}
    inPort.poll();
  } catch (std::string &err) {
    std::cout << "Error:" << err;
  }
}
