module emuInPort #(aw, dw)
   ( input	    clk,
     input           rst,
     output [dw-1:0] dout,
     input           re,
     output          dvalid );

   parameter MemSize = 1<<aw;
   parameter MemWidth = ((dw+31)>>5)<<5;

   reg [aw-1:0]      rp, p_rp;
   reg               p_dvalid;
   wire [aw-1:0]     rp_pl1;

   ////////////////////////////////////////////////////////////////////
   //
   // Memory Block
   //
   //pragma attribute mem flexmem cache fm_force 1 dmi 1
   reg [MemWidth-1:0]                 mem [(1<<aw)-1:0];

   // read operation
   wire [MemWidth-1:0] dout_w = mem[rp[aw-1:0]];
   assign dvalid = re & dout_w[MemWidth-1];
   assign dout = dout_w[dw-1:0];
   always @(posedge clk)
     if (p_dvalid)
       mem[p_rp[aw-1:0]] <= 'h0;

   /* Raise alarm to flush, keep track of number of flushes */
   wire                needsLoad = ~dout_w[MemWidth-1];
   reg [15:0]          flushCounter;
   always @(posedge clk)
     if (~rst) flushCounter <= 'h0;
     else if (needsLoad)
       flushCounter <= flushCounter + {15'h0, 1'b1};

   ////////////////////////////////////////////////////////////////////
   //
   // Misc Logic
   //
   always @(posedge clk)
     if (~rst) begin
	rp <= {aw{1'b0}};
	p_rp <= {aw{1'b0}};
        p_dvalid <= 1'b0;
     end else begin
       if (dvalid) begin
	  rp <= rp_pl1;
	  p_rp <= rp;
       end
        p_dvalid <= dvalid;
     end
   assign rp_pl1 = rp + { {(aw-1){1'b0}}, 1'b1 };

endmodule // emuInFifo
