`timescale 1ns/1ps

module testbench;

   bit            clk;
   reg            reset;

   //clock generator
   //tbx clkgen
   initial begin
      clk = 0;
      #7; //phase delay
      forever begin
         clk = 1;
         #5;
         clk = 0;
         #5;
      end
   end

   //reset generator
   //tbx clkgen
   initial begin
      reset = 0;
      #30;
      reset = 1;
   end

   //tbx clkgen
   initial begin
      #30000;
      $finish;
   end

   /* generate inputs to be sent */
   bit [39:0] dexp, dout;
   wire       dvalid;
   always @(posedge clk) begin
      dexp <= (~reset) ? 'h0 : dvalid ? dexp+1 : dexp;
      if (dexp != dout)
        $display("Missmatch: dexp:%d dout:%d", dexp, dout);
   end

   emuInPort #(.aw(8), .dw(40)) getIn
     ( .clk(clk),
       .rst(reset),
       .dout(dout),
       .re(1'b1),
       .dvalid(dvalid) );

   emu_env env(clk, reset);

   import "DPI-C" function void sw_poll();
   always @(posedge clk)
     if (reset & ((~|env.ts) | (getIn.needsLoad))) begin
        $display("Poll @%0d", env.ts);
        sw_poll();
     end

endmodule
