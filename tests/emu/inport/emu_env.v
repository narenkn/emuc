module emu_env(input fastest_clk, input rst_l);

   reg [`EMU_TS_WIDTH-1:0] ts;

   ////////////////////////////////////////////////////////////////////
   // Timestamp
   ////////////////////////////////////////////////////////////////////
   always @(posedge fastest_clk)
     if (~rst_l) ts <= `EMU_TS_WIDTH'h0;
     else ts <= ts + `EMU_TS_WIDTH'h1;
   
endmodule; // emu_env
