module emuOutPort #(aw, dw)
   ( input	    clk,
     input          rst,
     input [dw-1:0] din,
     input          we );

   parameter MemSize = 1<<aw;
   parameter MemWidth = (((dw+`EMU_TS_WIDTH+1)+31)>>5)<<5;

   ////////////////////////////////////////////////////////////////////
   //
   // Memory Block
   //
   //pragma attribute mem flexmem cache fm_force 1 dmi 1
   reg [MemWidth-1:0]                 mem [(1<<aw)-1:0];

   /* write pointers */
   reg [aw-1:0]     wp;
   wire [aw-1:0]    wp_pl1;

   /* Raise alarm to flush, keep track of number of flushes */
   wire [MemWidth-1:0] mem_wp_pl1 = mem[wp_pl1];
   wire                needsFlush = mem_wp_pl1[MemWidth-1];
   reg [15:0]              flushCounter;
   always @(posedge clk)
     if (~rst) flushCounter <= 'h0;
     else if (needsFlush)
       flushCounter <= flushCounter + {15'h0, 1'b1};

   // write operation
   always @(posedge clk)
     if (we)
       mem[wp[aw-1:0]] <= {1'b1, `EMU_ENV.ts, (MemWidth-`EMU_TS_WIDTH-1)'(din) };

   ////////////////////////////////////////////////////////////////////
   //
   // Misc Logic
   //
   always @(posedge clk)
     if (~rst)	wp <= {aw{1'b0}};
     else
       if (we)	wp <= wp_pl1;
   assign wp_pl1 = wp + { {(aw-1){1'b0}}, 1'b1 };

   /* Is next has valid data */
   wire [MemWidth-1:0]                nxtData = mem[wp_pl1];
   reg                                nxtIsVldData;
   always @(posedge clk)
     nxtIsVldData <= nxtData[MemWidth-1];

endmodule // emuOutFifo
