#include <cstdint>
#include <cstring>
#include <string>
#include <time.h>
#include <iostream>
#include <sstream>
#include <random>

#ifdef VCS
  void*
  scemi_mem_c_handle(const char *p) { return nullptr; }
  void
  scemi_mem_get_size(void* t_this, unsigned int *w, long long unsigned int *s) { *w = 64; *s = 100; }
  void scemi_mem_put_block(void* t_this, std::uint32_t start, std::uint32_t size, void *src) { }
  void scemi_mem_get_block(void* t_this, std::uint32_t start, std::uint32_t size, void *dest) { }
#else
  #include "tbxbindings.h"
  #include "svdpi.h"
  #include "stdio.h"
  #include "stdlib.h"
  #include "scemi_dmi.h"
#endif

#define STR_H(l) #l
#define STR(l) STR_H(l)

struct ReadBus {
  void fromInt32(void *b) {
    static std::uint32_t expVal = 0;
    static bool firstCall = true;
    std::uint32_t* d = (std::uint32_t *)b;
    if (firstCall)
      expVal = d[0];
    else if (expVal++ != d[0])
      std::cout << "Error: Expected:" << expVal << " Obtained data: " << d[0] << "\n";
  }
};

ReadBus rb;

#include "emuPorts.hh"

EmuOutPort<ReadBus> outPort{"testbench.getOut", 256, 64};

static bool first = true;
extern "C" void
sw_poll()
{
  try {
    if (first) outPort.init(), first = false;
    outPort.poll();
    while (outPort.pop(rb)) {}
  } catch (std::string &err) {
    std::cout << "Error:" << err;
  }
}
