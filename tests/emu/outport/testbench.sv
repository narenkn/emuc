`timescale 1ns/1ps

module testbench;

   bit            clk;
   reg            reset;

   //clock generator
   //tbx clkgen
   initial begin
      clk = 0;
      #7; //phase delay
      forever begin
         clk = 1;
         #5;
         clk = 0;
         #5;
      end
   end

   //reset generator
   //tbx clkgen
   initial begin
      reset = 0;
      #30;
      reset = 1;
   end

   //tbx clkgen
   initial begin
      #30000;
      $finish;
   end

   /* generate inputs to be sent */
   bit [39:0] din;
   always @(posedge clk)
     din <= (~reset) ? 8'h5 : din+1;

   emuOutPort #(.aw(8), .dw(40)) getOut
     ( .clk(clk),
       .rst(reset),
       .din(din),
       .we(1'b1) );

   emu_env env(clk, reset);

   import "DPI-C" function void sw_poll();
   always @(posedge clk)
     if (reset & ((~|env.ts) | (getOut.needsFlush))) begin
        $display("Poll @%0d", env.ts);
        sw_poll();
`ifdef SIMULATION
        for (int ui=0; ui<256; ui++) getOut.mem[ui] = 'h0;
`endif
     end

endmodule
