
package emuc;
   import "DPI-C" function int unsigned emuc_hash( string path );
   import "DPI-C" function void emuc_put( int unsigned id, inout bit[31:0] dat );
   import "DPI-C" function void emuc_get( int unsigned id, inout bit [31:0] dat );
   import "DPI-C" function int unsigned emuc_num(int unsigned id);
   import "DPI-C" function int unsigned emuc_register(string path);

   typedef struct packed {
      bit [7:0]  _addr;
      bit [7:0]  _data;
   } REQ;
   export "DPI-C" function emuc_req_get_trigger;
   event          emuc_req_received;
   function void emuc_req_get_trigger();
      -> emuc_req_received;
   endfunction // trigger
endpackage

class emucMailbox #(type T=bit[31:0]);
   int unsigned pipeId;
   function new(string PATH);
      pipeId = emuc::emuc_register(PATH);
      $display("Inst:%s pipeId:%h", PATH, pipeId);
   endfunction

   function int num();
      return emuc::emuc_num(pipeId);
   endfunction // num

   task get(ref T ival);
      while (0 == emuc::emuc_num(pipeId)) begin
        @(emuc::emuc_req_received);
      end
      emuc::emuc_get(pipeId, ival);
   endtask

   task put(ref T sval);
      emuc::emuc_put(pipeId, sval);
   endtask
endclass

module sendOutData(input bit clk);

   emuc::REQ req, reqo;
   emucMailbox #(emuc::REQ) intrans = new($psprintf("%m.intrans"));

   initial begin
      req._addr = 'hef;
      req._data = 'hba;
      intrans.put(req);
      $display("sent trans");
      intrans.get(reqo);
      $display("addr:%h data:%h", req._addr, req._data);
      $display("finish of sendOutData::initial");
      repeat(1000) @(posedge clk);
      $finish;
   end

endmodule // sendOutData

module sv2;

   bit clk;

   sendOutData so(clk);

   initial forever #5 clk = ~clk;

endmodule // tt
