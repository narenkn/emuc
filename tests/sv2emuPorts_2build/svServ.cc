#include <queue>
#include <iostream>
#include <unordered_map>
#include <boost/asio.hpp>
#include "scCommon.hh"
#if EMUC_SERVER
  #include "server.hh"
#endif
#if EMUC_CLIENT
  #include "client.hh"
#endif

#include <cstring>

#include "sdp_bus.cc"
#include "shash.hh"

static svScope emucScope;
extern "C" void emuc_req_get_trigger();

template<class Bus, std::uint32_t arraySize, std::uint32_t packedSize>
class EmuInPort : public Pipe
{
public:
  TransHeader header;
  svBitVecVal _dptr[arraySize][packedSize];
  std::uint32_t nxtIdx;
  EmuInPort(std::uint32_t pipeId) :
    Pipe{pipeId}
  {
    _data = (char *)&header;
    nxtIdx = 0;
    connect();
  }
  bool send(char *b) {
    if (nxtIdx >= arraySize) {
      return false;
    }
    for (std::uint32_t ui3=packedSize; ui3; ui3--) {
      _dptr[nxtIdx][ui3] = ((svBitVecVal*)b)[ui3];
    }
    nxtIdx++;
    return true;
  }
  void poll()
  {
    if (0 == nxtIdx) return;
    header.sizeOf = nxtIdx*packedSize*sizeof(svBitVecVal);
    rawSend((char *)&header, header.sizeOf + sizeof(header));
    nxtIdx = 0;
  }
  void receive(std::uint32_t sz) { }
};
template<class Bus, std::uint32_t arraySize, std::uint32_t packedSize>
class EmuOutPort : public Pipe
{
public:
  TransHeader header;
  svBitVecVal _dptr[arraySize][packedSize];
  std::queue<Bus> qData;
  EmuOutPort(std::uint32_t pipeId) :
    Pipe{pipeId}
  {
    _data = (char *)&header;
    connect();
    std::memset(_data, 0, sizeof(header)+(arraySize*packedSize*sizeof(svBitVecVal)));
  }
  void poll() { }
  void receive(std::uint32_t sz)
  {
    std::uint32_t nxtRecv;
    for (nxtRecv=0; sz >= (packedSize*sizeof(std::uint32_t));
         nxtRecv++, sz-=(packedSize*sizeof(std::uint32_t))) {
      qData.emplace(Bus());
      auto &b = qData.back();
      std::memcpy(b._val, _dptr+(nxtRecv*packedSize), packedSize*sizeof(svBitVecVal));
    }
    /* trigger SV side */
    if (not emucScope) {
      emucScope = svGetScopeFromName("emuc");
    }
    svSetScope(emucScope);
    emuc_req_get_trigger();
    //std::cout << "EmuTransactor::receive called\n";
  }
  bool get(char *d) {
    if (0 == qData.size()) return false;

    auto pack = qData.front();
    std::memcpy(d, pack._val, packedSize*sizeof(svBitVecVal));
    qData.pop();
    return true;
  }
  std::uint32_t num() { return qData.size(); }
};

extern "C"
std::uint32_t emuc_put(std::uint32_t pipeId, svBitVecVal *p)
{
  auto it = connection->getPipe(pipeId);
  return it->send((char *)p) ? 0 : 1;
}

extern "C"
std::uint32_t emuc_get(std::uint32_t pipeId, svBitVecVal *p)
{
  auto it = connection->getPipe(pipeId);
  return it->get((char *)p) ? 0 : 1;
}

extern "C"
std::uint32_t emuc_num(std::uint32_t pipeId)
{
  auto it = connection->getPipe(pipeId);
  return it->num();
}

extern "C"
std::uint32_t emuc_register(const char* path)
{
  std::uint32_t pipeId = shash::crc32_string(path);

  //std::cout << "emuc_req_num pipeId:" << std::hex << pipeId << "\n";
  auto it = _pipes->find(pipeId);
  if (it == _pipes->end()) {
    std::cout << "Warning: pipe(" << path << ") : Does not exist!!!!\n";
    return 0;
  }

  return pipeId;
}

struct _init {
  _init() {
    _pipes = std::make_unique<std::multimap<std::uint32_t, Pipe *>>();
    _unused_pipes = std::make_unique<std::map<std::uint32_t, std::unique_ptr<UnusedPipe>>>();
    connection = std::make_unique<Connection>();
    _pipes->emplace(CRC32_STR("sv2.so.intrans"), new EmuInPort<UmcSdpReqBus, 100, 1>(CRC32_STR("sv2.so.intrans")));
  }
} _init_i;
