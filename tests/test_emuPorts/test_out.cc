#include <cstdint>
#include <cstring>
#include <string>
#include <time.h>
#include <iostream>
#include <sstream>
#include <random>

#define STR(l) #l

struct TransHeader {
  std::uint32_t pipeId, sizeOf;
};

class Pipe
{
public:
  const std::uint32_t pipeId;
  char* _data;
  std::uint32_t expVal;
  void rawSend(char *p, std::uint32_t sz) {
    std::uint32_t* d = (std::uint32_t *)p;
    for (;sz; sz-=sizeof(std::uint32_t)) {
      if (expVal++ != (d[0] & ~0x80000000))
        std::cout << "Error: Expected:" << expVal << " Obtained data: " << (d[0]&~0x80000000) << "\n";
      d++;
    }
  }
  virtual void receive(std::uint32_t sz) = 0;
  virtual void poll() = 0;
  void connect() {}

  Pipe() = delete;
  Pipe(std::uint32_t p) : pipeId(p), expVal(0) { }
};

template <std::uint32_t s, std::uint32_t w>
struct Memory {
  std::uint32_t width, size;
  std::uint32_t mem[s][w>>5];
  std::uint32_t wp;
  Memory() : width(w), size(s) { wp = 0; }
  void incAddr(std::uint32_t& adr) {
    adr = (adr+1) % size;
  }
  bool push(std::uint32_t d) {
    if (mem[wp][(width>>5)-1] & 0x80000000) {
      std::cout << "Couldn't push:" << d << "\n";
      return false;
    }
    mem[wp][0] = 0x80000000 | d;
    incAddr(wp);
    return true;
  }
};

Memory<100, 32> Mr;

void*
scemi_mem_c_handle(const char *p) { return &Mr; }
void
scemi_mem_get_size(void* t_this, std::uint32_t *w, long long unsigned int *s)
{
  *w = Mr.width;
  *s = Mr.size;
}
void scemi_mem_put_block(void* t_this, std::uint32_t start, std::uint32_t size, void *src);
void scemi_mem_get_block(void* t_this, std::uint32_t start, std::uint32_t size, void *dest);

#include "emuPorts.hh"

class NotUsed {};
EmuOutPort<NotUsed, 100, 1> outPort{"abcd", 1234};

int
main(int argc, char *argv[])
{
  std::uint32_t rw_data=0;
  std::uint32_t loop, ui1;

  srand(time(0));
  outPort.connect();

  for (loop=1000000; loop; loop--) {
    ui1 = rand() % 101;
    for (; ui1; ui1--) {
      Mr.push(rw_data++);
    }
    outPort.poll();
  }

  std::cout << "@end rw_data: " << rw_data << " ExpVal:" << outPort.expVal << "\n";

  return 0;
}

void
scemi_mem_put_block(void* t_this, std::uint32_t start, std::uint32_t size, void *src)
{
  decltype(Mr) *mr = (decltype(Mr) *)t_this;
  std::memcpy(((std::uint32_t*)(mr->mem))+(start*(mr->width>>5)), src, size*(mr->width>>5)*sizeof(std::uint32_t));
}

void
scemi_mem_get_block(void* t_this, std::uint32_t start, std::uint32_t size, void *dest)
{
  decltype(Mr) *mr = (decltype(Mr) *)t_this;
  auto dmem = mr->mem;

  dmem = (decltype(dmem)) dest;
  std::uint32_t intInWord = mr->width >> 5;
  for (; size; size--, mr->incAddr(start)) {
    for (std::uint32_t ui1=intInWord; ui1; ui1--) {
      dmem[start][ui1-1] = mr->mem[start][ui1-1];
    }
  }
}
