#include <cstdint>
#include <cstring>
#include <string>
#include <time.h>
#include <iostream>
#include <sstream>
#include <random>

#define STR(l) #l

struct TransHeader {
  std::uint32_t pipeId, sizeOf;
};

class Pipe
{
public:
  const std::uint32_t pipeId;
  char* _data;
  void rawSend(char *p, std::uint32_t sz) {}
  virtual void receive(std::uint32_t sz) = 0;
  virtual void poll() = 0;
  void connect() {}

  Pipe() = delete;
  Pipe(std::uint32_t p) : pipeId(p) {}
};

template <std::uint32_t s, std::uint32_t w>
struct Memory {
  std::uint32_t width, size;
  std::uint32_t mem[s][w>>5];
  std::uint32_t rp;
  std::uint32_t expData;
  Memory() : width(w), size(s) { rp = 0; expData = 0; }
  void incAddr(std::uint32_t& adr) {
    adr = ((adr+1) >= size) ? 0 : adr+1;
  }
  void getAllData() {
    while (1) {
      if (mem[rp][(width>>5)-1] & 0x80000000) {
        if (expData != (mem[rp][0]&~0x80000000)) {
          std::cout << "Error: expected:" << expData << " got:" << (mem[rp][0]&~0x80000000) << "\n";
        }
        expData++;
        std::memset(((std::uint32_t*)mem)+(rp*(width>>5)), 0, (width>>5)*sizeof(std::uint32_t));
        incAddr(rp);
      } else
        break;
    }
  }
};

Memory<100, 32> Mw;

void*
scemi_mem_c_handle(const char *p) { return &Mw; }
void
scemi_mem_get_size(void* t_this, std::uint32_t *w, long long unsigned int *s)
{
  *w = Mw.width;
  *s = Mw.size;
}
void scemi_mem_put_block(void* t_this, std::uint32_t start, std::uint32_t size, void *src);
void scemi_mem_get_block(void* t_this, std::uint32_t start, std::uint32_t size, void *dest);

static std::uint32_t inVal=0;
void
assignData(std::uint32_t *b)
{
  b[0] = 0x80000000 | inVal++;
}

#include "emuPorts.hh"

class NotUsed {};
EmuInPort<NotUsed, 100, 1> inPort{"abcd", 1234};

int
main(int argc, char *argv[])
{
  std::uint32_t loop, ui1, ui2;

  srand(time(0));
  inPort.connect();

  for (loop=1000000; loop; loop--) {
    ui1=rand()%100;
    auto ui3 = ui1;
    for (ui2=0; ui1; ui1--, ui2++) {
      assignData(inPort.data+ui2);
    }
    inPort.receive(ui3*1*sizeof(std::uint32_t));
    inPort.poll();
    Mw.getAllData();
  }

  std::cout << "Last Exp:" << Mw.expData << " Sent:" << inVal << "\n";

  return 0;
}

void
scemi_mem_put_block(void* t_this, std::uint32_t start, std::uint32_t size, void *src)
{
  decltype(Mw) *mr = (decltype(Mw) *)t_this;
  std::memcpy(((std::uint32_t*)(mr->mem))+(start*(mr->width>>5)), src, size*(mr->width>>5)*sizeof(std::uint32_t));
}

void
scemi_mem_get_block(void* t_this, std::uint32_t start, std::uint32_t size, void *dest)
{
  decltype(Mw) *mr = (decltype(Mw) *)t_this;
  auto dmem = mr->mem;

  dmem = (decltype(dmem)) dest;
  std::uint32_t intInWord = mr->width >> 5;
  for (; size; size--, mr->incAddr(start)) {
    for (std::uint32_t ui1=intInWord; ui1; ui1--) {
      dmem[start][ui1-1] = mr->mem[start][ui1-1];
    }
  }
}
