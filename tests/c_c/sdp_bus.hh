#pragma once

struct UmcSdpReqBus : public BusData<1> {
  static fieldMap_t fm;
  virtual fieldMap_t& getFieldMap() {
    return fm;
  }
};
