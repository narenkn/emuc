#include "bus.cc"
#include "sdp_bus.hh"

Bus::fieldMap_t UmcSdpReqBus::fm = {
  {"addr", {7, 0, 0}},
  {"data", {15, 8, 0}},
};
