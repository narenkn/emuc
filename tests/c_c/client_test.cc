#include <chrono>
#include <thread>
#include <cstring>
#include <vector>
#include <queue>
#include <iostream>
#include <unordered_map>
#include <boost/asio.hpp>
#include "scCommon.hh"
#if EMUC_SERVER
  #include "server.hh"
#endif
#if EMUC_CLIENT
  #include "client.hh"
#endif
#include "shash.hh"
#include "svdpi.h"
#include "svPorts.hh"
#include "sdp_bus.cc"

typedef std::int32_t PLI_INT32;
typedef void* vpiHandle;
typedef void* p_cb_data;

struct t_cb_data *dummy_data;
extern "C" PLI_INT32 emuc_nextSimTime_callback(struct t_cb_data *cbd);
extern "C" PLI_INT32 EmucInit(p_cb_data cb_data_p);
extern "C" PLI_INT32 EmucFinish(p_cb_data cb_data_p);
extern "C" void emuc_poll_trigger();
extern "C" void emuc_received_trigger();

#define XXTERN extern "C"
#define PROTO_PARAMS(ARG) ARG

XXTERN PLI_INT32
vpi_control         PROTO_PARAMS((PLI_INT32 operation,
                                                    ...))
{
  return 0;
}

XXTERN vpiHandle  vpi_register_cb     PROTO_PARAMS((p_cb_data cb_data_p))
{
  return nullptr;
}

XXTERN PLI_INT32  vpi_free_object     PROTO_PARAMS((vpiHandle object))
{
  return 0;
}

XXTERN svScope svGetScopeFromName(const char* scopeName)
{
  return nullptr;
}

XXTERN svScope svSetScope(const svScope scope)
{
  return nullptr;
}

bool finish_set = false;

extern "C" void emuc_poll_trigger()
{ }

extern "C" void emuc_received_trigger()
{
  finish_set = true;
}

#define SLEEP1MS std::this_thread::sleep_for(std::chrono::milliseconds(1))

int
main()
{
  EmuOutPort<UmcSdpReqBus, 100, 1> p1(CRC32_STR("sv2.so.intrans"));
  UmcSdpReqBus b1;

  std::cout << "PipeId:" << std::hex << CRC32_STR("sv2.so.intrans") << std::dec << "\n";
  /* initial */
  EmucInit(dummy_data);
  SLEEP1MS;
  
  /* always */
  for (std::uint32_t ui1=1000; ui1; ui1--) {
    emuc_nextSimTime_callback(dummy_data);
    SLEEP1MS;
  }
  auto retval = emuc_get(p1.pipeId, b1._val);
  std::cout << "retval:" << retval << " obtained:" << std::hex << b1._val[0] << std::dec << "\n";

  /* initial */
  EmucFinish(dummy_data);
  SLEEP1MS;

  return 0;
}
