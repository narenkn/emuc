#include <chrono>
#include <thread>
#include <cstring>
#include <vector>
#include <queue>
#include <iostream>
#include <unordered_map>
#include <boost/asio.hpp>
#include "scCommon.hh"
#if EMUC_SERVER
  #include "server.hh"
#endif
#if EMUC_CLIENT
  #include "client.hh"
#endif
#include "shash.hh"
#include "svdpi.h"
#include "svPorts.hh"
#include "sdp_bus.cc"

typedef std::int32_t PLI_INT32;
typedef void* vpiHandle;
typedef void* p_cb_data;

struct t_cb_data *dummy_data;
extern "C" PLI_INT32 emuc_nextSimTime_callback(struct t_cb_data *cbd);
extern "C" PLI_INT32 EmucInit(p_cb_data cb_data_p);
extern "C" PLI_INT32 EmucFinish(p_cb_data cb_data_p);
extern "C" void emuc_poll_trigger();
extern "C" void emuc_received_trigger();

#define XXTERN extern "C"
#define PROTO_PARAMS(ARG) ARG

XXTERN PLI_INT32
vpi_control         PROTO_PARAMS((PLI_INT32 operation,
                                                    ...))
{
  return 0;
}

XXTERN vpiHandle  vpi_register_cb     PROTO_PARAMS((p_cb_data cb_data_p))
{
  return nullptr;
}

XXTERN PLI_INT32  vpi_free_object     PROTO_PARAMS((vpiHandle object))
{
  return 0;
}

XXTERN svScope svGetScopeFromName(const char* scopeName)
{
  return nullptr;
}

XXTERN svScope svSetScope(const svScope scope)
{
  return nullptr;
}

bool finish_set = false;

extern "C" void emuc_poll_trigger()
{ }

extern "C" void emuc_received_trigger()
{
  finish_set = true;
}

#define SLEEP1MS std::this_thread::sleep_for(std::chrono::milliseconds(1))

int
main()
{
  EmuInPort<UmcSdpReqBus, 100, 1> p1(CRC32_STR("sv2.so.intrans"));
  UmcSdpReqBus b1;

  /* initial */
  EmucInit(dummy_data);
  SLEEP1MS;

  /* always */
  bool first = true;
  while (not connection->hasError) {
    emuc_nextSimTime_callback(dummy_data);
    if (first) {
      b1._val[0] = 0x80000000 | 0x0ABCDEF9;
      emuc_put(p1.pipeId, b1._val);
      emuc_flush(p1.pipeId);
      first = false;
    }
    SLEEP1MS;
  }

  /* initial */
  EmucFinish(dummy_data);
  SLEEP1MS;

  return 0;
}
