#include <chrono>
#include <thread>
#include <cstring>
#include <vector>
#include <queue>
#include <iostream>
#include <unordered_map>
#include <boost/asio.hpp>
#include "vpi_user.h"
#include "svdpi.h"
#include "shash.hh"
#include "scCommon.hh"
#if EMUC_SERVER
  #include "server.hh"
#endif
#if EMUC_CLIENT
  #include "client.hh"
#endif
#include "svPorts.cc"

#include "bus.cc"

struct UmcSdpReqBus : public BusData<4> {
  static fieldMap_t fm;
  virtual fieldMap_t& getFieldMap() {
    return fm;
  }
};

Bus::fieldMap_t UmcSdpReqBus::fm = {
{"addr", {31, 0, 0} },
{"data", {63, 32, 0} },
{"abcd1", {95, 64, 0} },
{"abcd2", {127, 96, 0} },
};


struct t_cb_data *dummy_data;

#define SLEEP1MS std::this_thread::sleep_for(std::chrono::milliseconds(1))

/* flaky code : competes with 0-time initialization of
   library code as well, could fail later ... */
extern "C"
void _init_i_2()
{
#if EMUC_SERVER
  EmuOutPort<UmcSdpReqBus, 100, 4> *p1 = new EmuOutPort<UmcSdpReqBus, 100, 4>(CRC32_STR("sv2.so.intrans"));
  p1->connect();
#endif
#if EMUC_CLIENT
  EmuInPort<UmcSdpReqBus, 100, 4> *p2 = new EmuInPort<UmcSdpReqBus, 100, 4>(CRC32_STR("sv2.so.intrans"));
  p2->connect();
#endif
}
