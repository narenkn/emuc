`include "svPorts.sv"

typedef struct packed {
   bit [7:0]   _addr;
   bit [7:0]   _data;
} REQ;

import "DPI-C" function void _init_i_2();

module sendOutData(input bit clk);

   REQ req;
`ifdef SEND_DATA
   emuc::OutPort #(REQ) intrans;
`else
   emuc::InPort #(REQ) intrans;
`endif

   initial begin
      intrans = new($psprintf("%m.intrans"));
      _init_i_2();
      $display("after _init_i_2()");
`ifdef SEND_DATA
      req._addr = 'h9a;
      req._data = 'hfe;
      intrans.put(req);
      intrans.flush();
`else
      intrans.get(req);
      $display("Obtained addr:%h data:%h", req._addr, req._data);
`endif
      $display("finish of sendOutData::initial");
      repeat(10) @(posedge clk);
      $finish;
   end

endmodule // sendOutData

module sv2;

   bit clk;

   sendOutData so(clk);

   initial forever #5 clk = ~clk;

endmodule // tt
