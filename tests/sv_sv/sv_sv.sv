package emuc_SDP_REQ;
typedef struct packed {
   bit [31:0]   _addr;
   bit [31:0]   _data;
   bit [31:0]  abcd1;
   bit [31:0]  abcd2;
} REQ;
   import "DPI-C" function int unsigned emuc_put( int unsigned id, inout REQ dat );
   import "DPI-C" function int unsigned emuc_get( int unsigned id, output REQ dat );
endpackage

`include "svPorts.sv"

import "DPI-C" function void _init_i_2();

module sendOutData(input bit clk);

   emuc_SDP_REQ::REQ req;
`ifdef SEND_DATA
   emuc::OutPort #(emuc_SDP_REQ::REQ) intrans;
`else
   emuc::InPort #(emuc_SDP_REQ::REQ) intrans;
`endif

   initial begin
      intrans = new($psprintf("%m.intrans"));
      _init_i_2();
      $display("after _init_i_2()");
`ifdef SEND_DATA
      req._addr = 'h9a;
      req._data = 'hfe;
     req.abcd1 = 'h9b;
     req.abcd2 = 'h9d;
      intrans.put(req);
      intrans.flush();
     req._addr = 'haa;
     req._data = 'h55;
     intrans.put(req);
     intrans.flush();
`else
     intrans.get(req);
     $display("Obtained addr:%h data:%h abcd1:%h abcd2:%h", req._addr, req._data, req.abcd1, req.abcd2);
     intrans.get(req);
     $display("Obtained addr:%h data:%h abcd1:%h abcd2:%h", req._addr, req._data, req.abcd1, req.abcd2);
`endif
      $display("finish of sendOutData::initial");
      repeat(10) @(posedge clk);
      $finish;
   end

endmodule // sendOutData

module sv2;

   bit clk;
   integer env_ts = 5;

   sendOutData so(clk);

   initial forever #5 clk = ~clk;

endmodule // tt
