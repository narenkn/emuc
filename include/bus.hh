#pragma once

#include <cstdint>
#include <cassert>
#include <cstring>
#include <string>
#include <vector>
#include <unordered_map>
#include <sstream>
#include <iostream>
#include <svdpi.h>
#include <iomanip>
#include <algorithm>

struct Bus {
  svBitVecVal *val;
  struct Field {
    uint16_t msb, lsb, _lsb;
    Field(uint16_t m, uint16_t l, uint16_t _l) : msb(m), lsb(l), _lsb(_l) {}
  };
  typedef std::unordered_map<std::string, Field> fieldMap_t;
  std::string toHex(fieldMap_t::const_iterator& it);
  std::string monPrint();
  virtual fieldMap_t& getFieldMap() = 0;
};

template <std::uint32_t sz>
struct BusData : public Bus {
  svBitVecVal _val[sz];
  BusData() : Bus() {
    val = _val;
  }
  virtual fieldMap_t& getFieldMap() = 0;
};
