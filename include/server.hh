#pragma once

#define EMUC_SERVER_HH

#ifdef EMUC_CLIENT_HH
#error "Include either server.hh or client.hh!"
#endif

//----------------------------------------------------------------------
class SocketServer : public std::enable_shared_from_this<SocketServer>
{
public:
  bool isConnected{false};
  SocketServer(tcp::socket socket)
    : socket_(std::move(socket))
  {
    totWrSz = 0;
  }

  ~SocketServer() {
    connection_status++;
  }

  void start()
  {
    isConnected = true;
    do_read_header();
  }

  void write(std::shared_ptr<writeTrans> wp)
  {
    auto self(shared_from_this());
    //std::cout << "SocketServer::write(" << wp->sz << ") called\n";
//    std::cout << "SocketServer::write data:" << std::hex << ((std::uint32_t *)wp->p)[2] << std::dec << "\n";
    wip = wp;
    totWrSz = 0;
    boost::asio::async_write
      (socket_,
       boost::asio::buffer(wp->data.get(), wp->sz),
        [this, self](boost::system::error_code ec, std::size_t length)
        {
          //std::cout << "Wrote length:" << length << " pipeId:" << std::dec << " sizeof:" << length << "\n";
          totWrSz += length;
          //std::cout << "wip.sz:" << wip->sz << "\n";
          if (totWrSz >= wip->sz) {
            wip = nullptr;
            if (_pending_writes->size()) {
              write(_pending_writes->front());
              _pending_writes->pop();
            }
          }
          if (ec and not connection->hasError) {
            connection->hasError = true;
            std::cerr << "Error while Write Body ec(" << ec << "), pipeId(" << /*std::hex << write_msgs_.back()->header->pipeId << */ std::dec << ")\n";
          }
        });
  }

private:
  void do_read_header()
  {
    //std::cout << "SocketServer::do_read_header() called\n";
    auto self(shared_from_this());
    boost::asio::async_read(socket_,
        boost::asio::buffer((void *)&header, sizeof(header)),
        [this, self](boost::system::error_code ec, std::size_t length)
        {
          //std::cout << "SocketServer::do_read_header() pipeId obtained was :" << std::hex << header.pipeId << std::dec << " ec:" << ec << " length:" << length << " header.sizeof:" << header.sizeOf << "\n";
          if (0 == ec) {
            auto p = connection->getPipe(header.pipeId);
            if (0 == header.sizeOf) {
              std::stringstream ss;
              ss << "Header with size of 0 received for pipe:" << header.pipeId << "\n";
              throw ss.str();
            }
            if (p)
              do_read_body(p);
          } else if (ec == boost::asio::error::eof) {
          } else if (not connection->hasError) {
            connection->hasError = true;
            std::cerr << "Error while Read Header ec(" << ec << "), sizeof(pipeId)(" << std::hex << sizeof(header.pipeId) << std::dec << ") size(" << header.sizeOf << ") length(" << length << ")\n";
          }
        });
  }

  void do_read_body(Pipe *p)
  {
    auto self(shared_from_this());
    std::memcpy(p->_data, &header, sizeof(header));
    boost::asio::async_read
		(socket_, boost::asio::buffer(p->_data+sizeof(header), header.sizeOf),
       [this, self, p](boost::system::error_code ec, std::size_t length)
       {
	 //std::cout << "SocketServer::do_read_body() pipeId:" << std::hex << p->pipeId << std::dec << " ec:" << ec << " length:" << length << "\n";
         if (0 == ec) {
           auto range=_pipes->equal_range(p->pipeId);
           for (auto it=range.first; it!=range.second; ++it) {
	     it->second->receive(length);
           }
           do_read_header();
         } else if (ec == boost::asio::error::eof) {
         } else if (not connection->hasError) {
           connection->hasError = true;
           std::cerr << "Error while Read Body ec(" << ec << "), pipeId(" << std::hex << p->pipeId << std::dec << ")\n";
         }
       });
  }

public:
  std::uint32_t totWrSz;
  TransHeader header;
  tcp::socket socket_;
  std::shared_ptr<writeTrans> wip;
};
extern std::shared_ptr<SocketServer> ss;

//----------------------------------------------------------------------
class Server
{
public:
  Server(boost::asio::io_service& io_s, std::shared_ptr<tcp::endpoint> endpoint)
    : acceptor_{io_s, *endpoint}, socket_{io_s}
  {
    do_accept();
  }

private:
  void do_accept()
  {
    acceptor_.async_accept(socket_,
        [this](boost::system::error_code ec)
        {
          if (!ec)
          {
            ss = std::make_shared<SocketServer>(std::move(socket_));
            ss->start();
            connection_status++;
          }
//          do_accept();
        });
  }

  tcp::socket socket_;
  tcp::acceptor acceptor_;
};

extern "C" void pollOnce();
extern "C" void pollInit();
