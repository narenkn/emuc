#pragma once

template<class Bus, std::uint32_t arraySize, std::uint32_t packedSize>
class EmuInPort : public Pipe {
 public:
  TransHeader header;
  std::uint32_t data[arraySize*packedSize];
  std::uint32_t datap[arraySize*packedSize];
  std::uint32_t nxtLoadLoc;
  void* memp;
  struct loc {std::uint32_t d[packedSize];};
  std::vector<loc> pendTrans;
  std::string path;
  bool has_error;
  EmuInPort(std::string p, std::uint32_t pipeId):
    Pipe{pipeId}, path(p), memp(nullptr), has_error(false) { }
  void connect()
  {
    Pipe::connect();
      nxtLoadLoc = 0;
    _data = (char *)&header;

    /* init */
    memp = scemi_mem_c_handle(path.c_str());
    unsigned int width;
    long long unsigned int size;
    scemi_mem_get_size(memp, &width, &size);

    if ((width != (packedSize<<5)) or (size != arraySize)) {
      has_error = true;
      std::stringstream ss;
      ss << __FILE__ "(" STR(__LINE__) "): Mem size or width mismatch w-" << width << "/" << (packedSize<<5) << " s-" << size << "/" << arraySize << "\n";
      throw ss.str();
    }

    std::memset(data, 0, arraySize*packedSize*sizeof(std::uint32_t));
    scemi_mem_put_block(memp, 0, arraySize, (unsigned char *)data);
  }
  void poll() {
    std::uint32_t bend, totTrans;
    std::uint32_t *dpp, *dpp0;
    std::uint32_t numPendData = pendTrans.size();
    if (0 == numPendData) return;
    /* transfer blocks : max two blocks will be transferred */
    scemi_mem_get_block(memp, 0, arraySize, (unsigned char *)datap);
    /* block1 : get end location */
    for (bend=nxtLoadLoc, totTrans=0,
           dpp=datap+(nxtLoadLoc*packedSize)+(packedSize-1),
           dpp0=datap+(nxtLoadLoc*packedSize);
         (bend<arraySize) && (totTrans<pendTrans.size());
         bend++, dpp+=packedSize, dpp0+=packedSize, totTrans++) {
      if (dpp[0] & (1<<31)) break;
      /* */
      std::memcpy(dpp0, pendTrans[totTrans].d, packedSize*sizeof(std::uint32_t));
    }
    if (bend != nxtLoadLoc) { /* we can send one block */
      if ((bend-nxtLoadLoc) > numPendData) bend = nxtLoadLoc + numPendData;
      scemi_mem_put_block(memp, nxtLoadLoc, bend-nxtLoadLoc, (void *)(datap+(nxtLoadLoc*packedSize)));
      numPendData = ((bend-nxtLoadLoc) >= numPendData) ? 0 : numPendData - (bend-nxtLoadLoc);
    }
    if ( (0 == numPendData /* nothing more to send */) or
         (bend != arraySize) /* no space */ ) {
      incWriteLoc(nxtLoadLoc, bend-nxtLoadLoc);
    } else {
      std::uint32_t toIncNxtLoadLoc = bend-nxtLoadLoc;
      /* try to send second block */
      for (bend=0, dpp=datap+packedSize-1, dpp0=datap;
           (bend<arraySize) && (totTrans<pendTrans.size());
           bend++, totTrans++, dpp+=packedSize, dpp0+=packedSize) {
        if (bend == nxtLoadLoc) break;
        if (dpp[0] & (1<<31)) break;
        /* */
        std::memcpy(dpp0, pendTrans[totTrans].d, packedSize*sizeof(std::uint32_t));
      }
      if (bend) { /* second block */
        if (bend >= numPendData) bend = numPendData;
        scemi_mem_put_block(memp, 0, bend, (void *)datap);
        numPendData = (bend >= numPendData) ? 0 : numPendData-bend;
        std::memset(data, 0, bend*packedSize*sizeof(std::uint32_t));
        incWriteLoc(nxtLoadLoc, bend);
      }
      incWriteLoc(nxtLoadLoc, toIncNxtLoadLoc);
    }
    if (totTrans >= pendTrans.size())
      pendTrans.clear();
    else
      pendTrans.erase(pendTrans.begin(), pendTrans.begin()+totTrans);
  }
  void incWriteLoc(std::uint32_t& wl, std::uint32_t times=1) {
    wl += times;
    if (wl >= arraySize)
      wl %= arraySize;
  }
  void receive(std::uint32_t sz) {
    std::uint32_t ui1;
    for (ui1=0; sz >= (packedSize*sizeof(std::uint32_t));
         ui1++, sz-=(packedSize*sizeof(std::uint32_t))) {
      if (data[(ui1*packedSize)+packedSize-1]) {
        pendTrans.push_back(loc{});
        auto &l = pendTrans.back();
        for (std::uint32_t ui2=0; ui2<packedSize; ui2++) {
          l.d[ui2] = data[(ui1*packedSize)+ui2];
        }
      } else break;
    }
    std::memset(data, 0, ui1*packedSize*sizeof(std::uint32_t));
  }
};

template<class Bus, std::uint32_t arraySize, std::uint32_t packedSize>
class EmuOutPort : public Pipe {
public:
  TransHeader header;
  std::uint32_t data[arraySize*packedSize];
  std::uint32_t datap[arraySize*packedSize];
  std::uint32_t nxtLoadLoc;
  void* memp;
  std::string path;
  bool has_error;
 public:
  EmuOutPort(std::string p, std::uint32_t pipeId):
    Pipe{pipeId}, path(p), memp(nullptr), has_error(false) { }
  void connect()
  {
    Pipe::connect();
    nxtLoadLoc = 0;
    _data = (char *)&header;

    memp = scemi_mem_c_handle(path.c_str());
    unsigned int width;
    long long unsigned int size;
    scemi_mem_get_size(memp, &width, &size);

    if ((width != (packedSize<<5)) or (size != arraySize)) {
      has_error = true;
      std::stringstream ss;
      ss << __FILE__ "(" STR(__LINE__) "): Mem size or width mismatch w-" << width << "/" << (packedSize<<5) << " s-" << size << "/" << arraySize << "\n";
      throw ss.str();
    }

    std::memset(data, 0, arraySize*packedSize*sizeof(std::uint32_t));
    scemi_mem_put_block(memp, 0, arraySize, (unsigned char *)data);
  }
  void poll() {
    std::uint32_t bend, totTrans = 0;
    std::uint32_t *dpp;

    /* transfer blocks only once & process data */
    std::memset(data, 0, arraySize*packedSize*sizeof(std::uint32_t));
    scemi_mem_get_block(memp, 0, arraySize, (unsigned char *)datap);
    /* block1 : get end location */
    for (bend=nxtLoadLoc,
           dpp=datap+(nxtLoadLoc*packedSize)+(packedSize-1);
         bend<arraySize; bend++, dpp+=packedSize) {
      if (0 == (dpp[0] & (1<<31))) break;
    }
    if (bend != nxtLoadLoc) { /* we can read one block */
      totTrans += (bend-nxtLoadLoc);
      scemi_mem_put_block(memp, nxtLoadLoc, bend-nxtLoadLoc, (void *)(data+(nxtLoadLoc*packedSize)));
      std::memcpy(data, datap+(nxtLoadLoc*packedSize), (bend-nxtLoadLoc)*packedSize*sizeof(std::uint32_t));
    }
    if (bend == arraySize /* more data to read */ ) {
      /* try to send second block */
      for (bend=0, dpp=datap+packedSize-1;
           bend<nxtLoadLoc; bend++, dpp+=packedSize) {
        if (0 == (dpp[0] & (1<<31))) break;
      }
      if (bend) { /* second block */
        scemi_mem_put_block(memp, 0, bend, (void *)(data+(totTrans*packedSize)));
        std::memcpy(data+(totTrans*packedSize), datap, bend*packedSize*sizeof(std::uint32_t));
        totTrans += bend;
      }
    }
    incReadLoc(nxtLoadLoc, totTrans);
    rawSend((char *)data, totTrans*packedSize*sizeof(std::uint32_t));
  }
  void incReadLoc(std::uint32_t& wl, std::uint32_t times=1) {
    wl += times;
    if (wl >= arraySize)
      wl %= arraySize;
  }
  void receive(std::uint32_t sz) { }
};
