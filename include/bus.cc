#include "bus.hh"

std::string
Bus::toHex(fieldMap_t::const_iterator& it)
{
  std::ostringstream ss;

  ss << it->first << "=";

  /* retrieve value & send */
  svBitVecVal v;
  auto msb = it->second.msb;
  auto msbP = msb % (sizeof(svBitVecVal)<<3);
  auto lsb = it->second.lsb;
  auto lsbP = lsb % (sizeof(svBitVecVal)<<3);
  auto numVecVals = (msb - lsb + 1) / (sizeof(svBitVecVal)<<3) + ( ((msb-lsb+1)%(sizeof(svBitVecVal)<<3)) ? 1 : 0 );
  auto index = (lsb / (sizeof(svBitVecVal)<<3));
  std::vector <std::string> vs;
  for (; numVecVals; numVecVals--, index++) {
    /* loose lower bits */
    v = val[index]; v >>= lsbP;
    /* shove the next integer's lower bits upper to this */
    if (index < (msb/(sizeof(svBitVecVal)<<3))) {
      auto vt = val[index+1];
      vt <<= (sizeof(svBitVecVal)<<3) - lsbP;
      v |= vt;
    }
    /* loose unwanted msb, in last iter */
    if (1 == numVecVals) {
      v <<= (sizeof(svBitVecVal)<<3) - (msb - lsb + 1); v >>= (sizeof(svBitVecVal)<<3) - (msb - lsb + 1);
    }

    /* print */
    char cs[16];
    sprintf(cs, "%08x", v);
    //std::cout << "numVecVals:" << numVecVals << " idx:" << index << " cs:" << cs << std::endl;
    vs.emplace_back(cs);
  }

  /* the values are in reverse order */
  std::reverse(vs.begin(), vs.end());
  ss << msb << ":" << lsb << ":" << it->second._lsb << "'";
  for (auto &s : vs) {
    ss << s;
  }
  return ss.str();
}

std::string
Bus::monPrint()
{
  std::ostringstream ss;

  auto fm = getFieldMap();
  for (auto f=fm.cbegin(); f != fm.cend() ; ++f) {
    ss << toHex(f) << " ";
  }

  return ss.str();
}
