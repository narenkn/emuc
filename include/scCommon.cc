std::uint32_t connection_status = 0;
std::unique_ptr<std::multimap<std::uint32_t, Pipe *>> _pipes;
std::unique_ptr<std::map<std::uint32_t, std::unique_ptr<UnusedPipe>>> _unused_pipes;
std::unique_ptr<Connection> connection;
std::unique_ptr<std::queue<std::shared_ptr<writeTrans>>> _pending_writes;

void
Pipe::rawSend(char *p, std::uint32_t sz)
{
//  /* local send */
//  auto range=_pipes->equal_range(pipeId);
//  for (auto it=range.first; it!=range.second; ++it) {
//    if (this != it->second)
//      it->second->receive(p->_data);
//  }
  /* socket send */
  auto wrp = std::make_shared<writeTrans>();
  wrp->sz = sizeof(TransHeader)+sz;
  wrp->data = std::make_unique<char[]>(sizeof(TransHeader)+sz);
  TransHeader *h = (TransHeader *) (wrp->data.get());
  h->pipeId = pipeId;
  h->sizeOf = sz;
  std::memcpy(wrp->data.get()+sizeof(TransHeader), p, sz);
  if (ss && (0 == _pending_writes->size()) && not ss->wip) {
    ss->write(wrp);
  } else {
//    std::cout << "Pushed into Pending writes\n";
    _pending_writes->push(wrp);
  }
}

Pipe::Pipe(std::uint32_t p) :
  pipeId(p) {
  //std::cout << "Pipe created for " << std::hex << p << std::dec << " this:" << (void *)this << "\n";
  _pipes->emplace(p, this);
}

/* Local connection sits in 'pipes' */
void
Connection::addPipe(Pipe *p)
{
  if (_pipes->end() == _pipes->find(p->pipeId)) {
    //std::cout << "Pipe id:" << std::hex << p->pipeId << " p:" << p << std::dec << "\n";
    _pipes->emplace(p->pipeId, p);
  }
}

namespace shash {
  std::uint32_t
  crc32_string(const std::string& x)
  {
    std::uint32_t ret = 0xFFFFFFFF; /* crc32<-1> */
    for (auto& c: x)
      ret = (ret >> 8) ^ crc_table[(ret ^ c) & 0x000000FF];
    return ret ^ 0xFFFFFFFF;
  }
}

struct _init {
  _init() {
    if (!_pipes) {
      _pipes = std::make_unique<std::multimap<std::uint32_t, Pipe *>>();
      _unused_pipes = std::make_unique<std::map<std::uint32_t, std::unique_ptr<UnusedPipe>>>();
      connection = std::make_unique<Connection>();
      _pending_writes = std::make_unique<std::queue<std::shared_ptr<writeTrans>>>();
    }
  }
} _init_i;
