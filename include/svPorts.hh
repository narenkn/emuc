#pragma once

extern "C" void emuc_received_trigger();

template<class Bus, std::uint32_t arraySize, std::uint32_t packedSize>
class EmuInPort : public Pipe
{
public:
  TransHeader header;
  svBitVecVal _dptr[arraySize][packedSize];
  std::uint32_t nxtIdx;
  EmuInPort(std::uint32_t pipeId) : Pipe{pipeId} { }
  void connect()
  {
    _data = (char *)&header;
    nxtIdx = 0;
    Connection::addPipe(this);
  }
  bool send(char *b) {
    if (nxtIdx >= arraySize) {
      return false;
    }
    for (std::uint32_t ui3=0; ui3<packedSize; ui3++) {
      _dptr[nxtIdx][ui3] = ((svBitVecVal*)b)[ui3];
    }
    nxtIdx++;
    return true;
  }
  void poll()
  {
    static std::uint8_t call_count;
    /* initiate a write */
    if (ss and _pending_writes->size() and not ss->wip) {
      ss->write(_pending_writes->front());
      _pending_writes->pop();
    }
    /* flush only when full, or after some time */
    call_count++;
    if ((nxtIdx < arraySize) && call_count) return;
    flush();
  }
  void flush()
  {
    if (nxtIdx) {
      header = {pipeId, (std::uint32_t)(nxtIdx*packedSize*sizeof(svBitVecVal))};
      rawSend((char *)_dptr, header.sizeOf);
      nxtIdx = 0;
    }
  }
  void receive(std::uint32_t sz) { }
};

template<class Bus, std::uint32_t arraySize, std::uint32_t packedSize>
class EmuOutPort : public Pipe
{
  void _clearq(std::queue<Bus> &qData)
  {
    std::queue<Bus> empty;
    std::swap( qData, empty );
  }
public:
  TransHeader header;
  svBitVecVal _dptr[arraySize][packedSize];
  std::queue<Bus> qData;
  svScope emucScope;
  EmuOutPort(std::uint32_t pipeId) : Pipe{pipeId} { }
  void connect()
  {
    _data = (char *)&header;
    _clearq(qData);
    Connection::addPipe(this);
    std::memset(_data, 0, sizeof(header)+(arraySize*packedSize*sizeof(svBitVecVal)));
    emucScope = svGetScopeFromName("emuc");
  }
  void poll() { }
  void receive(std::uint32_t sz)
  {
    std::uint32_t nxtRecv;
    for (nxtRecv=0; sz >= (packedSize*sizeof(std::uint32_t));
         nxtRecv++, sz-=(packedSize*sizeof(std::uint32_t))) {
      qData.emplace(Bus());
      auto &b = qData.back();
      std::memcpy(b._val, _dptr+nxtRecv, packedSize*sizeof(svBitVecVal));
    }
    /* trigger SV side */
    svSetScope(emucScope);
    emuc_received_trigger();
  }
  bool get(char *d) {
    if (0 == qData.size()) return false;

    auto pack = qData.front();
    std::memcpy(d, pack._val, packedSize*sizeof(svBitVecVal));
    qData.pop();
    return true;
  }
  std::uint32_t num() { return qData.size(); }
};

extern "C"
std::uint32_t emuc_errors();
extern "C"
std::uint32_t emuc_put(std::uint32_t pipeId, svBitVecVal *p);
extern "C"
std::uint32_t emuc_get(std::uint32_t pipeId, svBitVecVal *p);
extern "C"
void emuc_flush(std::uint32_t pipeId);
extern "C"
std::uint32_t emuc_num(std::uint32_t pipeId);
extern "C"
std::uint32_t emuc_register(const char* path, std::uint32_t type);
