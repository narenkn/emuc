#pragma once

#define EMUC_CLIENT_HH

#ifdef EMUC_SERVER_HH
#error "Include either server.hh or client.hh!"
#endif

class SocketClient
{
public:
  SocketClient(boost::asio::io_service& io_service,
	       tcp::resolver::iterator endpoint_iterator):
    io_service_(io_service), socket_(io_service_)
  {
    do_connect(endpoint_iterator);
    totWrSz = 0;
  }

  ~SocketClient() {
    connection_status = 0;
  }

  void write(std::shared_ptr<writeTrans> wp)
  {
    wip = wp;
    totWrSz = 0;
    boost::asio::async_write
      (socket_,
       boost::asio::buffer(wp->data.get(), wp->sz),
        [this](boost::system::error_code ec, std::size_t length)
        {
          //std::cout << "Wrote length:" << length << " pipeId:" << std::dec << " sizeof:" << length << "\n";
          totWrSz += length;
          //std::cout << "wip.sz:" << wip->sz << "\n";
          if (totWrSz >= wip->sz) {
            wip = nullptr;
            if (_pending_writes->size()) {
              write(_pending_writes->front());
              _pending_writes->pop();
            }
          }
          if (ec and not connection->hasError) {
            connection->hasError = true;
            std::cerr << "Error while Write Body ec(" << ec << "), pipeId(" << /*std::hex << write_msgs_.back()->header->pipeId <<*/ std::dec << ")\n";
          }
        });
  }

  void close()
  {
    io_service_.post([this]() { socket_.close(); });
    connection_status++;
  }

private:
  void do_connect(tcp::resolver::iterator endpoint_iterator)
  {
    boost::asio::async_connect(socket_, endpoint_iterator,
        [this](boost::system::error_code ec, tcp::resolver::iterator)
        {
          if (!ec)
          {
            connection_status++;
            do_read_header();
          }
        });
  }

  void do_read_header()
  {
    //std::cout << "do_read_header called need:" << sizeof(header) << "\n";
    boost::asio::async_read(socket_,
        boost::asio::buffer((void *)&header, sizeof(header)),
        [this](boost::system::error_code ec, std::size_t length)
        {
          //std::cout << "SocketClient::do_read_header PipeId:" << std::hex << header.pipeId << std::dec << " read:" << length << " bytes & sizeOf:" << header.sizeOf << "\n";
          if (0 == ec) {
	    if (length == sizeof(header)) {
	      auto p = connection->getPipe(header.pipeId);
              if (0 == header.sizeOf) {
                std::stringstream ss;
                ss << "Header with size of 0 received for pipe:" << header.pipeId << "\n";
                throw ss.str();
              }
              if (p)
                do_read_body(p);
	    }
          } else if (ec == boost::asio::error::eof) {
          } else if (not connection->hasError) {
            connection->hasError = true;
            std::cerr << "Error while Read Header ec(" << ec << "), sizeof(header)(" << sizeof(header) << ") size(" << header.sizeOf << ") length(" << length << ")\n";
            socket_.close();
          }
        });
  }

  void do_read_body(Pipe *p)
  {
    std::memcpy(p->_data, &header, sizeof(header));
    boost::asio::async_read
		(socket_, boost::asio::buffer(p->_data+sizeof(header), header.sizeOf),
        [this, p](boost::system::error_code ec, std::size_t length)
        {
          //std::cout << "SocketClient::do_read_body bytes:" << length << "\n";
          if (0 == ec) {
            auto range=_pipes->equal_range(p->pipeId);
            for (auto it=range.first; it!=range.second; ++it) {
              it->second->receive(length);
            }
            do_read_header();
          } else if (ec == boost::asio::error::eof) {
          } else if (not connection->hasError) {
            connection->hasError = true;
            std::cerr << "Error while Read Body ec(" << ec << "), header.sizeOf:" << header.sizeOf << ") length(" << length << ")\n";
            socket_.close();
          }
        });
  }

public:
  std::uint32_t totWrSz;
  TransHeader header;
  boost::asio::io_service& io_service_;
  tcp::socket socket_;
  std::shared_ptr<writeTrans> wip;
};

extern std::unique_ptr<SocketClient> ss;
extern "C" void pollOnce();
extern "C" void pollInit();
