#pragma once

#define SERVER_PORT 9001

//----------------------------------------------------------------------
using boost::asio::ip::tcp;
class Connection;

struct TransHeader {
  std::uint32_t pipeId, sizeOf;
};

//----------------------------------------------------------------------
class Pipe
{
public:
  const std::uint32_t pipeId;
  char* _data;
  void rawSend(char *p, std::uint32_t sz);
  virtual void receive(std::uint32_t sz) = 0;
  virtual void poll() = 0;
  virtual bool send(char *b) { return false; }
  virtual bool get(char *d) { return false; }
  virtual void flush() { }
  virtual std::uint32_t num() { return 0; }
  virtual void connect() = 0;

  Pipe() = delete;
  Pipe(std::uint32_t p);
};

class UnusedPipe : public Pipe
{
public:
  void receive(std::uint32_t sz) { }
  void poll() { }
  void connect() { }
  UnusedPipe(std::uint32_t pi) :
    Pipe{pi} { }
};

//----------------------------------------------------------------------
extern std::uint32_t connection_status;
extern std::unique_ptr<std::multimap<std::uint32_t, Pipe *>> _pipes;
extern std::unique_ptr<std::map<std::uint32_t, std::unique_ptr<UnusedPipe>>> _unused_pipes;
class Connection
{
public:
  bool hasError = false;
  static void addPipe(Pipe* p);
  Pipe *getPipe(std::uint32_t pipeId) {
    auto it = _pipes->find(pipeId);
    if (_pipes->end() == it) return nullptr;
    return it->second;
//    /* create a new unused pipe */
//    if (_unused_pipes->end() == _unused_pipes->find(pipeId)) {
//      _unused_pipes->emplace(pipeId, std::make_unique<UnusedPipe>(pipeId));
//    }
//    return (*_unused_pipes)[pipeId].get();
  }
};
extern std::unique_ptr<Connection> connection;

/* Pending Transactions can be kept here */
struct writeTrans {
  std::uint32_t sz;
  std::unique_ptr<char[]> data;
};
extern std::unique_ptr<std::queue<std::shared_ptr<writeTrans>>> _pending_writes;
