module emuInPort #(aw, dw)
   ( input	    clk,
     input           rst,
     output [dw-1:0] dout,
     input           re,
     output          dvalid );

   parameter MemSize = 1<<aw;
   parameter MemWidth = ((dw+31)>>5)<<5;

   reg [aw-1:0]      rp, p_rp;
   reg               p_dvalid;
   wire [aw-1:0]     rp_pl1;

   ////////////////////////////////////////////////////////////////////
   //
   // Memory Block
   //
   //pragma attribute mem flexmem cache fm_force 1 dmi 1
   reg [MemWidth-1:0]                 mem [(1<<aw)-1:0];

   // read operation
   wire [MemWidth-1:0] dout_w = mem[rp[aw-1:0]];
   assign dvalid = re & dout_w[MemWidth-1];
   assign dout = dout_w[dw-1:0];
   always @(posedge clk)
     if (p_dvalid)
       mem[p_rp[aw-1:0]] <= 'h0;

   /* Raise alarm to flush, keep track of number of flushes */
   wire                needsLoad = ~dout_w[MemWidth-1];
   reg [15:0]          flushCounter;
   always @(posedge clk)
     if (~rst) flushCounter <= 'h0;
     else if (needsLoad)
       flushCounter <= flushCounter + {15'h0, 1'b1};

   ////////////////////////////////////////////////////////////////////
   //
   // Misc Logic
   //
   always @(posedge clk)
     if (~rst) begin
	rp <= {aw{1'b0}};
	p_rp <= {aw{1'b0}};
        p_dvalid <= 1'b0;
     end else begin
       if (dvalid) begin
	  rp <= rp_pl1;
	  p_rp <= rp;
       end
        p_dvalid <= dvalid;
     end
   assign rp_pl1 = rp + { {(aw-1){1'b0}}, 1'b1 };

endmodule // emuInFifo

module emuOutPort #(aw, dw)
   ( input	    clk,
     input          rst,
     input [dw-1:0] din,
     input          we );

   parameter MemSize = 1<<aw;
   parameter MemWidth = (((dw+`EMU_TS_WIDTH+1)+31)>>5)<<5;

   ////////////////////////////////////////////////////////////////////
   //
   // Memory Block
   //
   //pragma attribute mem flexmem cache fm_force 1 dmi 1
   reg [MemWidth-1:0]                 mem [(1<<aw)-1:0];

   /* write pointers */
   reg [aw-1:0]     wp;
   wire [aw-1:0]    wp_pl1;

   /* Raise alarm to flush, keep track of number of flushes */
   wire [MemWidth-1:0] mem_wp_pl1 = mem[wp_pl1];
   wire                needsFlush = mem_wp_pl1[MemWidth-1];
   reg [15:0]              flushCounter;
   always @(posedge clk)
     if (~rst) flushCounter <= 'h0;
     else if (needsFlush)
       flushCounter <= flushCounter + {15'h0, 1'b1};

   // write operation
   always @(posedge clk)
     if (we)
       mem[wp[aw-1:0]] <= {1'b1, `EMU_ENV.ts, (MemWidth-`EMU_TS_WIDTH-1)'(din) };

   ////////////////////////////////////////////////////////////////////
   //
   // Misc Logic
   //
   always @(posedge clk)
     if (~rst)	wp <= {aw{1'b0}};
     else
       if (we)	wp <= wp_pl1;
   assign wp_pl1 = wp + { {(aw-1){1'b0}}, 1'b1 };

   /* Is next has valid data */
   wire [MemWidth-1:0]                nxtData = mem[wp_pl1];
   reg                                nxtIsVldData;
   always @(posedge clk)
     nxtIsVldData <= nxtData[MemWidth-1];

endmodule // emuOutFifo

package emuc;
   `define EMUC_OUT_PORT_TYPE 1
   `define EMUC_IN_PORT_TYPE 2
   import "DPI-C" function int unsigned emuc_errors();
   import "DPI-C" function int unsigned emuc_hash( string path );
   import "DPI-C" function int unsigned emuc_put( int unsigned id, inout bit[31:0] dat );
   import "DPI-C" function int unsigned emuc_get( int unsigned id, inout bit[31:0] dat );
   import "DPI-C" function void emuc_flush( int unsigned id );
   import "DPI-C" function int unsigned emuc_num(int unsigned id);
   import "DPI-C" function int unsigned emuc_register(string path, int unsigned t);
   import "DPI-C" function int unsigned emuc_connection_status();
   export "DPI-C" function emuc_received_trigger;
   export "DPI-C" function emuc_poll_trigger;
   event          ev_received, ev_poll;
   function void emuc_received_trigger();
      -> ev_received;
   endfunction
   function void emuc_poll_trigger();
      -> ev_poll;
   endfunction

   task wait4connection();
      forever begin
         if (emuc::emuc_connection_status()) break;
         @(emuc::ev_poll);
      end
   endtask

   class OutPort #(type T=bit[31:0]);
      int            unsigned pipeId;
      function new(string PATH);
         pipeId = emuc::emuc_register(PATH, `EMUC_OUT_PORT_TYPE);
      endfunction

      task put(ref T sval);
         int unsigned retval;
         forever begin
            retval = emuc::emuc_put(pipeId, sval);
            if (retval) break;
            @(emuc::ev_poll);
         end
      endtask
      function int unsigned try_put(ref T sval);
         return emuc::emuc_put(pipeId, sval);
      endfunction
      function void flush();
         emuc::emuc_flush(pipeId);
      endfunction
   endclass

   class InPort #(type T=bit[31:0]);
      int            unsigned pipeId;
      function new(string PATH);
         pipeId = emuc::emuc_register(PATH, `EMUC_IN_PORT_TYPE);
      endfunction

      function int num();
         return emuc::emuc_num(pipeId);
      endfunction // num

      task get(ref T ival);
         while (0 == emuc::emuc_get(pipeId, ival)) begin
            @(emuc::ev_received);
         end
      endtask
   endclass
   `undef EMUC_OUT_PORT_TYPE
   `undef EMUC_IN_PORT_TYPE
endpackage
