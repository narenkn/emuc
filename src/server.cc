//
// chat_server.cpp
// ~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2016 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at <a href="http://www.boost.org/LICENSE_1_0.txt">http://www.boost.org/LICENSE_1_0.txt</a>)
//

#include <vector>
#include <queue>
#include <iostream>
#include <unordered_map>
#include <boost/exception/all.hpp>
#include <boost/throw_exception.hpp>
#include <boost/asio.hpp>
#ifndef HW_EMUL
#include "vpi_user.h"
#include "svdpi.h"
#endif
#include "shash.hh"
#include "scCommon.hh"
#include "server.hh"

#include "shash.hh"

#include "scCommon.cc"

std::unique_ptr<boost::asio::io_service> io_service;
std::unique_ptr<Server> serverp;
std::shared_ptr<SocketServer> ss;

extern "C" void
pollOnce()
{
  for (auto it: *_pipes) {
    it.second->poll();
  }
  io_service->poll();
}

extern "C" void
pollInit()
{
  io_service = std::make_unique<boost::asio::io_service>();
  serverp = std::make_unique<Server>(*io_service, std::make_shared<tcp::endpoint>(tcp::v4(), SERVER_PORT));
  for (auto it: *_pipes) {
    it.second->connect();
  }
}

extern "C"
std::uint32_t emuc_hash(const char *p)
{
  return CRC32_STRING(p);
}
