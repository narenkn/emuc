#include <cstring>
#include <queue>
#include <iostream>
#include <unordered_map>
#include <boost/asio.hpp>
#include "vpi_user.h"
#include "svdpi.h"
#include "shash.hh"
#include "scCommon.hh"
#if EMUC_SERVER
  #include "server.hh"
#endif
#if EMUC_CLIENT
  #include "client.hh"
#endif
#include "svPorts.hh"

std::uint32_t emuc_errors()
{
  return connection->hasError ? 1 : 0;
}

std::uint32_t emuc_put(std::uint32_t pipeId, svBitVecVal *p)
{
  auto it = connection->getPipe(pipeId);
  if (it == nullptr) return 0;
  return it->send((char *)p) ? 1 : 0;
}

std::uint32_t emuc_get(std::uint32_t pipeId, svBitVecVal *p)
{
  Pipe *port= connection->getPipe(pipeId);
  if (port == nullptr) return 0;
  return port->get((char *)p) ? 1 : 0;
}

void emuc_flush(std::uint32_t pipeId)
{
  auto it = connection->getPipe(pipeId);
  if (it)
    it->flush();
}

std::uint32_t emuc_num(std::uint32_t pipeId)
{
  auto it = connection->getPipe(pipeId);
  if (! it) return 0;
  return it->num();
}

std::uint32_t emuc_register(const char* path, std::uint32_t type)
{
  std::uint32_t pipeId = shash::crc32_string(path);

  return pipeId;
}

extern "C"
std::uint32_t emuc_connection_status()
{
  return connection_status;
}
