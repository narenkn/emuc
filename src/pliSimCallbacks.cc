#include <string>
#include <vector>
#include <queue>
#include <iostream>
#include <unordered_map>
#include <boost/exception/all.hpp>
#include <boost/throw_exception.hpp>
#include <boost/asio.hpp>
#ifndef HW_EMUL
#include "vpi_user.h"
#include "svdpi.h"
#include "acc_user.h"
#include "vpi_user.h"
#else
typedef std::int32_t PLI_INT32;
typedef std::int32_t *p_cb_data;
typedef std::int32_t s_cb_data;
#endif
#include "shash.hh"
#include "scCommon.hh"
#if EMUC_SERVER
  #include "server.hh"
#endif
#if EMUC_CLIENT
  #include "client.hh"
#endif

#include "shash.hh"

extern "C" void pollOnce();
extern "C" void pollInit();

#ifndef HW_EMUL
static void emuc_nextSimTime_helper();

static svScope emucScope = 0;
#endif
extern "C" void emuc_poll_trigger();
extern "C" void emuc_received_trigger();

//----------------------------------------------------------------------
/* VpiConnector
 */
class VpiConnector : public Pipe
{
  bool finishSet;
  TransHeader header;
  std::uint32_t recv_command[2];
  std::uint32_t send_command[2];
public:
  void connect()
  {
    _data = (char *)&header;
    send_command[0] = send_command[1] = 0; /* connect command */
    rawSend((char *)send_command, sizeof(std::uint32_t)<<1);
  }
  void poll() { }
  /* will be called on connect
     command:
     0 : connect
   */
  void receive(std::uint32_t sz)
  {
    /* add socket pipe */
    //std::cout << "VpiConnector::receive called pi:" << std::hex << header.pipeId << " cmd[0]:" << recv_command[0] << "\n";
    /* */
    if (0 == recv_command[0]) {
#ifndef HW_EMUL
      svSetScope(emucScope);
      emuc_received_trigger();
#endif
    } else if (0 != recv_command[0]) { /* command to finish */
      if (not finishSet) {
        finishSet = true;
#ifndef HW_EMUL
        vpi_control(vpiFinish, 0);
#endif
      }
    }
  }

  void
  sendFinish()
  {
    finishSet = true;
  }

  VpiConnector(): Pipe{ CRC32_STR("//connect//") } {
    finishSet = false;
  }
};

std::unique_ptr<VpiConnector> pipe0;

extern "C"
PLI_INT32
emuc_nextSimTime_callback
(struct t_cb_data *cbd)
{
  pollOnce();
#ifndef HW_EMUL
  emuc_nextSimTime_helper();
  /* trigger sv side for poll */
  svSetScope(emucScope);
  emuc_poll_trigger();
#endif
  /* */
  return 0;
}

#ifndef HW_EMUL
static
void
emuc_nextSimTime_helper()
{
  s_cb_data  cbData;
  cbData.reason    = cbNextSimTime;
  cbData.cb_rtn    = emuc_nextSimTime_callback;
  cbData.time      = NULL;
  cbData.value     = NULL;
  cbData.obj       = NULL;
  cbData.user_data = NULL;

  vpiHandle cbHandle = vpi_register_cb(&cbData);
  vpi_free_object(cbHandle);
}
#endif

extern "C" PLI_INT32
EmucInit(p_cb_data cb_data_p)
{
  pipe0 = std::make_unique<VpiConnector>();
  pollInit();
#ifndef HW_EMUL
  emucScope = svGetScopeFromName("emuc");
  emuc_nextSimTime_helper();
#endif
  return 0;
}

extern "C" PLI_INT32
EmucFinish(p_cb_data cb_data_p)
{
//  pipe0.sendFinish();
  return 0;
}

#ifndef HW_EMUL
extern "C" PLI_INT32
EmucElabCb(void)
{
  s_cb_data callback;

  callback.reason = cbStartOfSimulation;
  callback.cb_rtn = EmucInit;
  callback.user_data = 0;
  vpiHandle cbHandle = vpi_register_cb(&callback);
  vpi_free_object(cbHandle);

  callback.reason = cbEndOfSimulation;
  callback.cb_rtn = EmucFinish;
  callback.user_data = 0;
  cbHandle = vpi_register_cb(&callback);
  vpi_free_object(cbHandle);

  return 0;
}
#endif
