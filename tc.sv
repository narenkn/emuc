
package emuc;
   typedef struct {
      bit [31:0]  pipeId;
      bit [31:0]  byteSize;
   } Header;
   typedef struct {
      emuc::Header h;
      bit [63:0]  _addr;
      bit [63:0]  _data;
   } REQ;
   parameter REQ_size = 16;
   import "DPI-C" function bit[31:0] emuc_hash( string path );
   export "DPI-C" function emuc_req_get_trigger;
   event          emuc_req_received;
   function void emuc_req_get_trigger();
      -> emuc_req_received;
   endfunction // trigger
   import "DPI-C" function void emuc_req_put( bit[31:0] id, inout REQ dat );
   import "DPI-C" function void emuc_req_get( bit[31:0] id, inout REQ dat );
   import "DPI-C" function bit[31:0] emuc_req_num(bit[31:0] id);
endpackage


class emucMailbox #(string PATH, type T=bit[31:0], int unsigned bySize);
   local bit[31:0] pipeId;
   function new();
      pipeId = emuc::emuc_hash(PATH);
      //$display("pipeId:%h", pipeId);
   endfunction

   function int num();
      return emuc::emuc_req_num(pipeId);
   endfunction // num

   task get(ref T ival);
      while (0 == emuc::emuc_req_num(pipeId)) begin
        @(emuc::emuc_req_received);
      end
      emuc::emuc_req_get(pipeId, ival);
   endtask

   task put(ref T sval);
      sval.h.pipeId = pipeId;
      sval.h.byteSize = bySize;
      emuc::emuc_req_put(pipeId, sval);
   endtask
endclass

module sendOutData;

   emuc::REQ req, reqo;
   int unsigned retVal;
   emucMailbox #("abcd", emuc::REQ, 16) intrans = new;

   initial begin
      req._addr = 'h0123_4567_89ab_cdef;
      req._data = 'hfedc_ba89_7654_3210;
      intrans.put(req);
      $display("sent trans");
      intrans.get(reqo);
      $display("finish of sendOutData::initial");
   end

endmodule // sendOutData

module tc;

   sendOutData so();

   bit clk;
   initial forever #5 clk = ~clk;

   initial begin
      forever begin
         @(posedge clk);
         $display(so.intrans.num());
      end
      $finish;
   end

endmodule // tc
